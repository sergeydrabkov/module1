﻿using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
           Console.WriteLine("--SwapItems--");
           Module1 m1 = new Module1(); 
           foreach (int a in m1.SwapItems(1, 2))
            {
                Console.WriteLine(a);
            }
           Console.WriteLine("--GetMinimumValue--");
           Console.WriteLine(m1.GetMinimumValue(new int[] {10, 20, 1}));
        }

        public int[] SwapItems(int a, int b)
        {
            return new int [] {b, a};
        }

        public int GetMinimumValue(int[] input)
        {
            int min = input[0];

            foreach (int i in input)
            {
                if (i < min)
                {
                    min = i;
                }
            }
            return min;
        }
    }
}
